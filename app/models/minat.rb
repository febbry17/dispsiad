class Minat < ActiveRecord::Base
  self.table_name = 'minat'
  validates :alam_terbuka, :seni, :pelayanan_sosial, :klerical, :teknik_mekanik, :hitungan, :kepustakaan, :ilmiah_sain, :id_csiswa, presence: true, on: :update
  belongs_to :siswa, foreign_key: :id_csiswa, primary_key: :id_csiswa
  NUMBER_OF_FIELD = 8

  def hasil(pencabangan)
    res = Pembobotan.where(tipe: "Minat", pencabangan: pencabangan)
    
    hash = {
      :alam_terbuka => res.where(aspek: "Alam Terbuka (Outdoor)").first.bobot, :seni => res.where(aspek: "Seni (Art)").first.bobot, 
      :pelayanan_sosial => res.where("aspek LIKE (?)", "%Pelayanan Sosial%").first.bobot, :klerical => res.where(aspek: "Klerikal (Clerical)").first.bobot ,
      :teknik_mekanik => res.where(aspek: "Teknik Mekanik (Mechanical)").first.bobot, :hitungan => res.where(aspek: "Hitungan (Computional)").first.bobot, 
      :kepustakaan => res.where(aspek: "Kepustakaan (Literary)").first.bobot, :ilmiah_sain => res.where(aspek: "Ilmiah Sain (Science)").first.bobot}
  end

  def hasil_pembobotan(pencabangan)
  	arr0 = []
  	hasil(pencabangan).values.each do |val| 
  		arr1 = []
  		arr2 = []
  		hasil(pencabangan).values.each{|val2| arr1 << val2.to_f/val.to_f}
  		arr1.each{|val2| arr2 << val2.to_f/arr1.sum}
  		# hasil.values.each{|val2| arr2 << val2.to_f/arr1.sum}
  		
  		arr0 << arr2
  	end

  	arr3 = []
  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr3 << x/NUMBER_OF_FIELD

  	end
  	
	arr4 = []

  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr4 << (x/NUMBER_OF_FIELD) / arr3.sum 
  	end
  	arr4
  end

  def nilai(pencabangan)
  	arr = [self.alam_terbuka, self.seni, self.pelayanan_sosial, self.klerical, self.teknik_mekanik, self.hitungan, self.kepustakaan, self.ilmiah_sain]
	 arr2 = []     
    arr_pemb = hasil_pembobotan(pencabangan)

    arr.each_with_index do |val, idx|
      h_normalisasi = (val.to_f / arr.max.to_f)
      arr2 <<  h_normalisasi * arr_pemb[idx]
    end
    
    arr2.sum > 0 ? ('%.3g' % arr2.sum) : ''
  end


  def ranking(pencabangan)
  	arr = []
  	arr4.each_with_index do |val, i|
  		arr << (hasil_pembobotan(pencabangan)[i] * nilai(pencabangan)[i])
  	end
  	arr.sum
  end
end
