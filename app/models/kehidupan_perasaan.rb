class KehidupanPerasaan < ActiveRecord::Base
  self.table_name = 'kehidupan_perasaan'
  validates :kepekaan, :penempatan_diri, :kepercayaan_diri, :kemandirian, :persuasif, :agresivitas, :pengelolaan_dorongan, :pengelolaan_emosi, :id_csiswa, presence: true, on: :update
  belongs_to :siswa, foreign_key: :id_csiswa, primary_key: :id_csiswa

  NUMBER_OF_FIELD = 8


  def hasil(pencabangan)

    res = Pembobotan.where(tipe: "Kehidupan Perasaan", pencabangan: pencabangan)
    hash = {
      :kepekaan => res.where(aspek: "Kepekaan").first.bobot, :penempatan_diri => res.where(aspek: "Penempatan Diri").first.bobot, 
      :kepercayaan_diri => res.where(aspek: "Kepercayaan Diri").first.bobot, :kemandirian => res.where(aspek: "Kemandirian").first.bobot, 
      :persuasif => res.where(aspek: "Persuasif").first.bobot, :agresivitas => res.where(aspek: "Agresivitas").first.bobot, 
      :pengelolaan_dorongan => res.where(aspek: "Pengelolaan Dorongan").first.bobot, :pengelolaan_emosi => res.where(aspek: "Pengelolaan Emosi").first.bobot}
    
  end

  def hasil_pembobotan(pencabangan)
  	arr0 = []
  	hasil(pencabangan).values.each do |val| 
  		arr1 = []
  		arr2 = []
  		hasil(pencabangan).values.each{|val2| arr1 << val2.to_f/val.to_f}
  		arr1.each{|val2| arr2 << val2.to_f/arr1.sum}
  		# hasil.values.each{|val2| arr2 << val2.to_f/arr1.sum}
  		
  		arr0 << arr2
  	end

  	arr3 = []
  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr3 << x/NUMBER_OF_FIELD

  	end
  	
	arr4 = []

  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr4 << (x/NUMBER_OF_FIELD) / arr3.sum 
  	end
  	arr4
  end

  def nilai(pencabangan)
  	arr = [self.kepekaan, self.penempatan_diri, self.kepercayaan_diri, self.kemandirian, self.persuasif, self.agresivitas, self.pengelolaan_dorongan, self.pengelolaan_emosi]
	 arr2 = []     
    arr_pemb = hasil_pembobotan(pencabangan)

    arr.each_with_index do |val, idx|
      h_normalisasi = (val.to_f / arr.max.to_f)
      arr2 <<  h_normalisasi * arr_pemb[idx]
    end
    
    arr2.sum > 0 ? ('%.3g' % arr2.sum) : ''
  end


  def ranking(pencabangan)
  	arr = []
  	arr4.each_with_index do |val, i|
  		arr << (hasil_pembobotan(pencabangan)[i] * nilai(pencabangan)[i])
  	end
  	arr.sum
  end
 end
