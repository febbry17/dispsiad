class Kecerdasan < ActiveRecord::Base
  self.table_name = 'kecerdasan'
  validates :pemahaman, :mengingat, :analisa, :konstruktif, :mengolah_angka, :aritmatika, :orientasi_ruang, :verbal, :estimasi, :tehnik_mekanik, :tehnik_elektronik, :wawasan, :id_csiswa, presence: true, on: :update
  belongs_to :siswa, foreign_key: :id_csiswa, primary_key: :id_csiswa
  NUMBER_OF_FIELD = 12

  def hasil(pencabangan)
    # b_pemahaman = if pemahaman <= 103
    #   1
    # elsif pemahaman.in?(104..111)
    #   2
    # elsif pemahaman.in?(112..119)
    #   3
    # elsif pemahaman.in?(120..127)
    #   4
    # elsif pemahaman >= 118
    #   5
    # end

    # b_mengingat = if mengingat <= 99
    #   1
    # elsif mengingat.in?(100..109)
    #   2
    # elsif mengingat.in?(110..118)
    #   3
    # elsif mengingat.in?(119..128)
    #   4
    # elsif mengingat > 128
    #   5
    # end

    # b_analisa = if analisa <= 88
    #   1
    # elsif analisa.in?(89..99)
    #   2
    # elsif analisa.in?(100..109)
    #   3
    # elsif analisa.in?(110..119)
    #   4
    # elsif analisa > 119
    #   5
    # end

    # b_konstruktif = if konstruktif <= 88
    #   1
    # elsif konstruktif.in?(89..101)
    #   2
    # elsif konstruktif.in?(102..113)
    #   3
    # elsif konstruktif.in?(114..126)
    #   4
    # elsif konstruktif > 126
    #   5
    # end

    # b_mengolah_angka = if mengolah_angka <= 82
    #   1
    # elsif mengolah_angka.in?(83..94)
    #   2
    # elsif mengolah_angka.in?(95..106)
    #   3
    # elsif mengolah_angka.in?(107..118)
    #   4
    # elsif mengolah_angka > 118
    #   5
    # end

    # b_aritmatika = if aritmatika <= 82
    #   1
    # elsif aritmatika.in?(83..94)
    #   2
    # elsif aritmatika.in?(95..106)
    #   3
    # elsif aritmatika.in?(107..118)
    #   4
    # elsif aritmatika > 118
    #   5
    # end

    # b_orientasi_ruang = if orientasi_ruang <= 88
    #   1
    # elsif orientasi_ruang.in?(89..101)
    #   2
    # elsif orientasi_ruang.in?(102..113)
    #   3
    # elsif orientasi_ruang.in?(114..126)
    #   4
    # elsif orientasi_ruang > 126
    #   5
    # end

    # b_verbal = if verbal <= 103
    #   1
    # elsif verbal.in?(104..111)
    #   2
    # elsif verbal.in?(112..119)
    #   3
    # elsif verbal.in?(120..127)
    #   4
    # elsif verbal >= 118
    #   5
    # end

    # b_estimasi = if estimasi <= 92
    #   1
    # elsif estimasi.in?(93..98)
    #   2
    # elsif estimasi.in?(99..104)
    #   3
    # elsif estimasi.in?(105..110)
    #   4
    # elsif estimasi > 110
    #   5
    # end

    # b_tehnik_mekanik = if tehnik_mekanik.in?(0..13)
    #   1
    # elsif tehnik_mekanik.in?(14..27)
    #   2
    # elsif tehnik_mekanik.in?(28..41)
    #   3
    # elsif tehnik_mekanik.in?(42..55)
    #   4
    # elsif tehnik_mekanik.in?(56..68)
    #   5
    # end

    # b_tehnik_elektronik = if tehnik_elektronik.in?(0..13)
    #   1
    # elsif tehnik_elektronik.in?(14..27)
    #   2
    # elsif tehnik_elektronik.in?(28..41)
    #   3
    # elsif tehnik_elektronik.in?(42..55)
    #   4
    # elsif tehnik_elektronik.in?(56..68)
    #   5
    # end

    # b_wawasan = if wawasan <= 90
    #   1
    # elsif wawasan.in?(91..100)
    #   2
    # elsif wawasan.in?(101..110)
    #   3
    # elsif wawasan.in?(111..119)
    #   4
    # elsif wawasan > 119
    #   5
    # end
        

    res = Pembobotan.where(tipe: "Kecerdasan", pencabangan: pencabangan)
    
    hash = {
      :pemahaman => res.where(aspek: "Kemampuan Pemahaman").first.bobot, :mengingat => res.where(aspek: "Kemampuan Mengingat").first.bobot, 
      :analisa => res.where(aspek: "Kemampuan Analisa").first.bobot, :konstruktif => res.where(aspek: "Kemampuan Konstruktif").first.bobot,
      :mengolah_angka => res.where(aspek: "Kemampuan Mengolah Angka (Numerik)").first.bobot, :aritmatika => res.where(aspek: "Kemampuan Aritmatika").first.bobot, 
      :orientasi_ruang => res.where(aspek: "Kemampuan Orientasi Ruang/Bidang").first.bobot, :verbal => res.where(aspek: "Kemampuan Verbal (Bahasa)").first.bobot,
      :estimasi => res.where(aspek: "Kemampuan Estimasi").first.bobot, :tehnik_mekanik => res.where(aspek: "Kemampuan Tehnik Mekanik").first.bobot, 
      :tehnik_elektronik => res.where(aspek: "Kemampuan Tehnik Elektro").first.bobot, :wawasan => res.where(aspek: "Kemampuan Wawasan").first.bobot
    }

  end

  def hasil_pembobotan(pencabangan = "INF")
  	arr0 = []
  	hasil(pencabangan).values.each do |val| 
  		arr1 = []
  		arr2 = []
  		hasil(pencabangan).values.each{|val2| arr1 << val2.to_f/val.to_f}
  		arr1.each{|val2| arr2 << val2.to_f/arr1.sum}
  		# hasil.values.each{|val2| arr2 << val2.to_f/arr1.sum}
  		
  		arr0 << arr2
  	end

  	arr3 = []
  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr3 << x/NUMBER_OF_FIELD

  	end
  	
	arr4 = []

  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr4 << (x/NUMBER_OF_FIELD) / arr3.sum 
  	end
  	arr4
  end

  def nilai(pencabangan = "INF")
  	arr = [self.pemahaman, self.mengingat, self.analisa, self.konstruktif, self.mengolah_angka, self.aritmatika, self.orientasi_ruang, self.verbal, 
  		self.estimasi, self.tehnik_mekanik, self.tehnik_elektronik, self.wawasan]
  	arr2 = []  		
    arr_pemb = hasil_pembobotan(pencabangan)

  	arr.each_with_index do |val, idx|
      h_normalisasi = (val.to_f / arr.max.to_f)
  		arr2 <<  h_normalisasi * arr_pemb[idx]
  	end
	  
    arr2.sum > 0 ? ('%.3g' % arr2.sum) : ''
  end


  def ranking(pencabangan = "INF")
  	arr = []
  	arr4.each_with_index do |val, i|
  		arr << (hasil_pembobotan(pencabangan)[i] * nilai(pencabangan)[i])
  	end
  	arr.sum
  end

  def ceil2(exp = 0)
   multiplier = 10 ** exp
   ((self * multiplier).ceil).to_f/multiplier.to_f
  end

end