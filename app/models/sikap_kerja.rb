class SikapKerja < ActiveRecord::Base
  self.table_name = 'sikap_kerja'
  validates :inisiatif, :kreatif, :perencanaan, :energi, :konsentrasi, :kecepatan, :ketelitian, :ketekunan, :keuletan, :daya_tahan, :kepemimpinan, :kerjasama, :toleransi_terhadap_stress, :id_csiswa, presence: true, on: :update
  belongs_to :siswa, foreign_key: :id_csiswa, primary_key: :id_csiswa
    NUMBER_OF_FIELD = 13

  def hasil(pencabangan)
  	res = Pembobotan.where(tipe: "Sikap Kerja", pencabangan: pencabangan)
    

	{:inisiatif => res.where(aspek: "Inisiatif").first.bobot, :kreatif => res.where(aspek: "Kreatif").first.bobot, :perencanaan => res.where(aspek: "Perencanaan").first.bobot,
	 :energi => res.where(aspek: "Energi").first.bobot, :konsentrasi => res.where(aspek: "Konsentrasi").first.bobot, :kecepatan => res.where(aspek: "Kecepatan (Tempo)").first.bobot, 
   :ketelitian => res.where(aspek: "Ketelitian").first.bobot, :ketekunan => res.where(aspek: "Ketekunan").first.bobot, :keuletan => res.where(aspek: "Keuletan").first.bobot, 
   :daya_tahan => res.where(aspek: "Daya Tahan").first.bobot, :kepemimpinan => res.where(aspek: "Kepemimpinan").first.bobot, :kerjasama => res.where(aspek: "Kerjasama").first.bobot, :toleransi_terhadap_stress => res.where(aspek: "Toleransi Terhadap Stress").first.bobot}  	
  end

  def pembobotan(min_val, max_val, val)
  	range = min_val..max_val

  	range.each_slice(range.last/4).with_index.with_object({}) { |(a,i),h|
  	  h[a.first..a.last]=i }
  	enum = range.each_slice(range.last/4)
  	
  	enum.each_with_index do |v, i|
  		return i+1 if val.in?(v)
  	end
  end

    def hasil_pembobotan(pencabangan)
  	arr0 = []
  	hasil(pencabangan).values.each do |val| 
  		arr1 = []
  		arr2 = []
  		hasil(pencabangan).values.each{|val2| arr1 << val2.to_f/val.to_f}
  		arr1.each{|val2| arr2 << val2.to_f/arr1.sum}
  		# hasil.values.each{|val2| arr2 << val2.to_f/arr1.sum}
  		
  		arr0 << arr2
  	end

  	arr3 = []
  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr3 << x/NUMBER_OF_FIELD

  	end
  	
	arr4 = []

  	arr0.each_with_index do |val, i|
  		x = 0
  		arr0.each_with_index {|val2, i2| x += arr0[i2][i] }
  		arr4 << (x/NUMBER_OF_FIELD) / arr3.sum 
  	end
  	arr4
  end

  def nilai(pencabangan)
  	arr = [self.inisiatif, self.kreatif, self.perencanaan, self.energi, self.konsentrasi, self.kecepatan, self.ketelitian, self.ketekunan, self.keuletan, self.daya_tahan, self.kepemimpinan, self.kerjasama, self.toleransi_terhadap_stress]
	 arr2 = []     
    arr_pemb = hasil_pembobotan(pencabangan)

    arr.each_with_index do |val, idx|
      h_normalisasi = (val.to_f / arr.max.to_f)
      arr2 <<  h_normalisasi * arr_pemb[idx]
    end

    arr2.sum > 0 ? ('%.3g' % arr2.sum) : ''
  end


  def ranking(pencabangan)
    arr = []
    arr4.each_with_index do |val, i|
      arr << (hasil_pembobotan(pencabangan)[i] * nilai(pencabangan)[i])
    end
    arr.sum
  end
end
