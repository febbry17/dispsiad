class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  PENDIDIKANLIST = ["SMA", "SMK", "MA"]
  
  devise :database_authenticatable, :registerable,
          :validatable

end
