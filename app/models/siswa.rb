class Siswa < ActiveRecord::Base
  self.table_name = 'siswa'
  self.primary_key = 'id_csiswa'

  validates :nama_lengkap, :tempat_lahir, :tanggal_lahir, :tg_badan, :b_badan, :agama, :pendidikan, :minat_kec, presence: true

  has_one :kecerdasan, primary_key: :id_csiswa, foreign_key: :id_csiswa
  has_one :kehidupan_perasaan, primary_key: :id_csiswa, foreign_key: :id_csiswa
  has_one :sikap_kerja, primary_key: :id_csiswa, foreign_key: :id_csiswa
  has_one :minat, primary_key: :id_csiswa, foreign_key: :id_csiswa

  after_create :init_childs
  accepts_nested_attributes_for :kecerdasan
  accepts_nested_attributes_for :kehidupan_perasaan
  accepts_nested_attributes_for :sikap_kerja
  accepts_nested_attributes_for :minat  

  before_save :init_usia
  before_update :set_rekomendasi_kecabangan


  def init_usia
    begin
      self.usia = (Date.today - self.tanggal_lahir.to_date).to_i / 356
      self.tgl_pendaftaran = Date.today
    rescue
    end
  end

  def init_childs
    Kecerdasan.create(id_csiswa: self.id)
    KehidupanPerasaan.create(id_csiswa: self.id)
    SikapKerja.create(id_csiswa: self.id)
    Minat.create(id_csiswa: self.id)
  end       

  def set_rekomendasi_kecabangan
    if kecerdasan.pemahaman
      rests = Pembobotan.pluck(:pencabangan).uniq
      arr = []
      selected_rec = nil
      tmp_arr = []
      rests.each do |res|
        new_arr = [kecerdasan.nilai(res), sikap_kerja.nilai(res), kehidupan_perasaan.nilai(res), minat.nilai(res)]
        if(tmp_arr.blank? && selected_rec.blank?)
          selected_rec = res
          tmp_arr = new_arr
        elsif((tmp_arr <=> new_arr) == -1)
          selected_rec = res
          tmp_arr = new_arr
        end

      end
      self.rekomendasi_kecabangan = selected_rec  #this is indicate that siswa already input their numbers
    end
  end  


end
