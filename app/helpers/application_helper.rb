module ApplicationHelper
  def bootstrap_class_for flash_type
        { success: "alert-success", error: "alert-error", alert: "alert-warning", notice: "alert-info" }[flash_type.to_sym] || flash_type.to_s
  end

  def flash_messages(opts = {})
    flash.each do |msg_type, message|
      concat(content_tag(:div, message, class: "alert #{bootstrap_class_for(msg_type)} fade in") do 
              concat content_tag(:button, 'x', class: "close", data: { dismiss: 'alert' })
              concat message.html_safe
            end)
    end
    nil
  end

  def range_of_value(r)
     grades = {
      "1" => (0..19),
      "2" => (20..39),
      "3" => (40..59),
      "4" => (60..79),
      "5" => (80..100),
      } 
    grade = grades.find{|key, range| range.include?(r) }
    grade.nil? ? "Unknown" : grade.first


  end
end
