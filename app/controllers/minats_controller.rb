class MinatsController < ApplicationController
  before_action :set_minat, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @minats = Minat.all
    respond_with(@minats)
  end

  def show
    respond_with(@minat)
  end

  def new
    @minat = Minat.new
    respond_with(@minat)
  end

  def edit
  end

  def create
    @minat = Minat.new(minat_params)
    @minat.save
    respond_with(@minat)
  end

  def update
    @minat.update(minat_params)
    respond_with(@minat)
  end

  def destroy
    @minat.destroy
    respond_with(@minat)
  end

  private
    def set_minat
      @minat = Minat.find(params[:id])
    end

    def minat_params
      params.require(:minat).permit(:alam_terbuka, :seni, :pelayanan_sosial, :klerical, :teknik_mekanik, :hitungan, :kepuasan, :kepustakaan, :ilmiah_sain)
    end
end
