class SiswasController < ApplicationController
  before_action :set_siswa, only: [:show, :edit, :update, :destroy, :psikotest, :hasil, :pencabangan]

  respond_to :html

  def index
    if request.xhr?
      @q = Siswa.ransack(params[:q])
      @siswas = @q.result(distinct: true)
      render layout: false 
    end
  end

  def show
    @siswa.tanggal_lahir = @siswa.tanggal_lahir.strftime("%d-%m-%Y")
    respond_with(@siswa)
  end

  def pencabangan
    render layout: false if request.xhr?
  end
  
  def psikotest
    render layout: false if request.xhr?

  end

  def hasil
    if request.xhr?
      par = params[:pencabangan] || "INF"
      @minat = @siswa.minat.hasil_pembobotan(par)
      @kecerdasan = @siswa.kecerdasan.hasil_pembobotan(par)
      @kehidupan_perasaan = @siswa.kehidupan_perasaan.hasil_pembobotan(par)
      @sikap_kerja = @siswa.sikap_kerja.hasil_pembobotan(par)
      render layout: false
    end
  end

  def new
    @siswa = Siswa.new
    @siswa.id_csiswa = Siswa.last.try(:id_csiswa).to_i + 1
    respond_with(@siswa)
  end

  def edit
    @siswa.tgl_pendaftaran = @siswa.tgl_lahir.strftime("%d-%m-%Y")
  end

  def laporan
    if params[:id]
      @siswa = Siswa.find_by(id_csiswa: params[:id])
      render pdf: "laporan_siswa", layout: false,:margin => { :bottom => 20 }
    else
      @siswas = Siswa.all
      render pdf: "laporan_keseluruhan_siswa", layout: false,:margin => { :bottom => 20 }
    end
  end

  def create
    @siswa = Siswa.new(siswa_params)
    @siswa.id = Siswa.last.try(:id).to_i + 1
    
    if @siswa.save
      flash[:notice] = "Data berhasil disimpan."
      redirect_to siswas_path
    else
      redirect_to :back, notice: "Data gagal disimpan, periksa kembali inputan."
    end
  end

  def update
    if @siswa.update(siswa_params)
      flash[:notice] = "Data berhasil disimpan."
      redirect_to (request.referer.include?('psikotest') ? psikotest_siswa_path(@siswa) : siswas_path)
    else
      redirect_to :back, notice: "Data gagal disimpan, periksa kembali inputan."
    end
    # respond_with(@siswa)
  end

  def destroy
    @siswa.destroy
    respond_with(@siswa)
  end

  private
    def set_siswa
      @siswa = Siswa.find_by(id_csiswa: params[:id])
    end

    def siswa_params
      params.require(:siswa).permit!
    end
end
