class KehidupanPerasaansController < ApplicationController
  before_action :set_kehidupan_perasaan, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @kehidupan_perasaans = KehidupanPerasaan.all
    respond_with(@kehidupan_perasaans)
  end

  def show
    respond_with(@kehidupan_perasaan)
  end

  def new
    @kehidupan_perasaan = KehidupanPerasaan.new
    respond_with(@kehidupan_perasaan)
  end

  def edit
  end

  def create
    @kehidupan_perasaan = KehidupanPerasaan.new(kehidupan_perasaan_params)
    @kehidupan_perasaan.save
    respond_with(@kehidupan_perasaan)
  end

  def update
    @kehidupan_perasaan.update(kehidupan_perasaan_params)
    respond_with(@kehidupan_perasaan)
  end

  def destroy
    @kehidupan_perasaan.destroy
    respond_with(@kehidupan_perasaan)
  end

  private
    def set_kehidupan_perasaan
      @kehidupan_perasaan = KehidupanPerasaan.find(params[:id])
    end

    def kehidupan_perasaan_params
      params.require(:kehidupan_perasaan).permit(:kepekaan, :penempatan_diri, :kepercayaan_diri, :kemandirian, :persuasif, :agresivitas, :pengelolaan_dorongan, :pengelolaan_emosi)
    end
end
