class PembobotansController < ApplicationController
  before_action :set_pembobotan, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @pembobotans = Pembobotan.all
    respond_with(@pembobotans)
  end

  def show
    respond_with(@pembobotan)
  end

  def new
    @pembobotan = Pembobotan.new
    respond_with(@pembobotan)
  end

  def edit
  end

  def create
    @pembobotan = Pembobotan.new(pembobotan_params)
    @pembobotan.save
    respond_with(@pembobotan)
  end

  def update
    @pembobotan.update(pembobotan_params)
    respond_with(@pembobotan)
  end

  def destroy
    @pembobotan.destroy
    respond_with(@pembobotan)
  end

  private
    def set_pembobotan
      @pembobotan = Pembobotan.find(params[:id])
    end

    def pembobotan_params
      params.require(:pembobotan).permit(:aspek, :pencabangan, :bobot)
    end
end
