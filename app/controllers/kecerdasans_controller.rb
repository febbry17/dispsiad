class KecerdasansController < ApplicationController
  before_action :set_kecerdasan, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @kecerdasans = Kecerdasan.all
    respond_with(@kecerdasans)
  end

  def show
    respond_with(@kecerdasan)
  end

  def new
    @kecerdasan = Kecerdasan.new
    respond_with(@kecerdasan)
  end

  def edit
  end

  def create
    @kecerdasan = Kecerdasan.new(kecerdasan_params)
    @kecerdasan.save
    respond_with(@kecerdasan)
  end

  def update
    if !@kecerdasan.update(kecerdasan_params)
      flash[:notice] = "Data gagal disimpan, periksa kembali inputan."
    end
    redirect_to psikotest_siswa_path(@kecerdasan.siswa)
  end

  def destroy
    @kecerdasan.destroy
    respond_with(@kecerdasan)
  end

  private
    def set_kecerdasan
      @kecerdasan = Kecerdasan.find(params[:id])
    end

    def kecerdasan_params
      params.require(:kecerdasan).permit(:pemahaman, :mengingat, :analisa, :konstruktif, :mengolah_angka, :aritmatika, :orientasi_ruang, :verbal, :estimasi, :tehnik_mekanik, :tehnik_elektronik, :wawasan)
    end
end
