class SikapKerjasController < ApplicationController
  before_action :set_sikap_kerja, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @sikap_kerjas = SikapKerja.all
    respond_with(@sikap_kerjas)
  end

  def show
    respond_with(@sikap_kerja)
  end

  def new
    @sikap_kerja = SikapKerja.new
    respond_with(@sikap_kerja)
  end

  def edit
  end

  def create
    @sikap_kerja = SikapKerja.new(sikap_kerja_params)
    @sikap_kerja.save
    respond_with(@sikap_kerja)
  end

  def update
    if !@sikap_kerja.update!(sikap_kerja_params)
      flash[:notice] = "Data gagal disimpan, periksa kembali inputan."
    end
    redirect_to psikotest_siswa_path(@sikap_kerja.siswa)
  end

  def destroy
    @sikap_kerja.destroy
    respond_with(@sikap_kerja)
  end

  private
    def set_sikap_kerja
      @sikap_kerja = SikapKerja.find(params[:id])
    end

    def sikap_kerja_params
      params.require(:sikap_kerja).permit(:inisiatif, :kreatif, :perencanaan, :energi, :konsentrasi, :kecepatan, :ketelitian, :ketekunan, :keuletan, :daya_tahan, :kepemimpinan, :kerjasama, :toleransi_terhadap_stress)
    end
end
