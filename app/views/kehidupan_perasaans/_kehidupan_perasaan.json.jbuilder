json.extract! kehidupan_perasaan, :id, :kepekaan, :penempatan_diri, :kepercayaan_diri, :kemandirian, :persuasif, :agresivitas, :pengelolaan_dorongan, :pengelolaan_emosi, :created_at, :updated_at
json.url kehidupan_perasaan_url(kehidupan_perasaan, format: :json)
