json.extract! siswa, :id, :nama_lengkap, :tempat_lahir, :tinggi_badan, :berat_badan, :usia, :agama, :riwayat_pendidikan, :minat_kecabangan, :created_at, :updated_at
json.url siswa_url(siswa, format: :json)
