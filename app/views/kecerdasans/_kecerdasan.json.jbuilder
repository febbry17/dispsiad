json.extract! kecerdasan, :id, :pemahaman, :mengingat, :analisa, :konstruktif, :mengolah_angka, :aritmatika, :orientasi_ruang, :verbal, :estimasi, :tehnik_mekanik, :tehnik_elektronik, :wawasan, :created_at, :updated_at
json.url kecerdasan_url(kecerdasan, format: :json)
