json.extract! sikap_kerja, :id, :inisiatif, :kreatif, :perencanaan, :energi, :konsentrasi, :kecepatan, :ketelitian, :ketekunan, :keuletan, :daya_tahan, :kepemimpinan, :kerjasama, :toleransi_terhadap_stress, :created_at, :updated_at
json.url sikap_kerja_url(sikap_kerja, format: :json)
