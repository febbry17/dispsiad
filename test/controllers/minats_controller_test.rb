require 'test_helper'

class MinatsControllerTest < ActionController::TestCase
  setup do
    @minat = minats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:minats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create minat" do
    assert_difference('Minat.count') do
      post :create, minat: { alam_terbuka: @minat.alam_terbuka, hitungan: @minat.hitungan, ilmiah_sain: @minat.ilmiah_sain, kepuasan: @minat.kepuasan, kepustakaan: @minat.kepustakaan, klerical: @minat.klerical, pelayanan_sosial: @minat.pelayanan_sosial, seni: @minat.seni, teknik_mekanik: @minat.teknik_mekanik }
    end

    assert_redirected_to minat_path(assigns(:minat))
  end

  test "should show minat" do
    get :show, id: @minat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @minat
    assert_response :success
  end

  test "should update minat" do
    patch :update, id: @minat, minat: { alam_terbuka: @minat.alam_terbuka, hitungan: @minat.hitungan, ilmiah_sain: @minat.ilmiah_sain, kepuasan: @minat.kepuasan, kepustakaan: @minat.kepustakaan, klerical: @minat.klerical, pelayanan_sosial: @minat.pelayanan_sosial, seni: @minat.seni, teknik_mekanik: @minat.teknik_mekanik }
    assert_redirected_to minat_path(assigns(:minat))
  end

  test "should destroy minat" do
    assert_difference('Minat.count', -1) do
      delete :destroy, id: @minat
    end

    assert_redirected_to minats_path
  end
end
