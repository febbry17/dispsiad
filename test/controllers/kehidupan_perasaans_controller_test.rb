require 'test_helper'

class KehidupanPerasaansControllerTest < ActionController::TestCase
  setup do
    @kehidupan_perasaan = kehidupan_perasaans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kehidupan_perasaans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kehidupan_perasaan" do
    assert_difference('KehidupanPerasaan.count') do
      post :create, kehidupan_perasaan: { agresivitas: @kehidupan_perasaan.agresivitas, kemandirian: @kehidupan_perasaan.kemandirian, kepekaan: @kehidupan_perasaan.kepekaan, kepercayaan_diri: @kehidupan_perasaan.kepercayaan_diri, penempatan_diri: @kehidupan_perasaan.penempatan_diri, pengelolaan_dorongan: @kehidupan_perasaan.pengelolaan_dorongan, pengelolaan_emosi: @kehidupan_perasaan.pengelolaan_emosi, persuasif: @kehidupan_perasaan.persuasif }
    end

    assert_redirected_to kehidupan_perasaan_path(assigns(:kehidupan_perasaan))
  end

  test "should show kehidupan_perasaan" do
    get :show, id: @kehidupan_perasaan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kehidupan_perasaan
    assert_response :success
  end

  test "should update kehidupan_perasaan" do
    patch :update, id: @kehidupan_perasaan, kehidupan_perasaan: { agresivitas: @kehidupan_perasaan.agresivitas, kemandirian: @kehidupan_perasaan.kemandirian, kepekaan: @kehidupan_perasaan.kepekaan, kepercayaan_diri: @kehidupan_perasaan.kepercayaan_diri, penempatan_diri: @kehidupan_perasaan.penempatan_diri, pengelolaan_dorongan: @kehidupan_perasaan.pengelolaan_dorongan, pengelolaan_emosi: @kehidupan_perasaan.pengelolaan_emosi, persuasif: @kehidupan_perasaan.persuasif }
    assert_redirected_to kehidupan_perasaan_path(assigns(:kehidupan_perasaan))
  end

  test "should destroy kehidupan_perasaan" do
    assert_difference('KehidupanPerasaan.count', -1) do
      delete :destroy, id: @kehidupan_perasaan
    end

    assert_redirected_to kehidupan_perasaans_path
  end
end
