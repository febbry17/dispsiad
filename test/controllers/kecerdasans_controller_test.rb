require 'test_helper'

class KecerdasansControllerTest < ActionController::TestCase
  setup do
    @kecerdasan = kecerdasans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kecerdasans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kecerdasan" do
    assert_difference('Kecerdasan.count') do
      post :create, kecerdasan: { analisa: @kecerdasan.analisa, aritmatika: @kecerdasan.aritmatika, estimasi: @kecerdasan.estimasi, konstruktif: @kecerdasan.konstruktif, mengingat: @kecerdasan.mengingat, mengolah_angka: @kecerdasan.mengolah_angka, orientasi_ruang: @kecerdasan.orientasi_ruang, pemahaman: @kecerdasan.pemahaman, tehnik_elektronik: @kecerdasan.tehnik_elektronik, tehnik_mekanik: @kecerdasan.tehnik_mekanik, verbal: @kecerdasan.verbal, wawasan: @kecerdasan.wawasan }
    end

    assert_redirected_to kecerdasan_path(assigns(:kecerdasan))
  end

  test "should show kecerdasan" do
    get :show, id: @kecerdasan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kecerdasan
    assert_response :success
  end

  test "should update kecerdasan" do
    patch :update, id: @kecerdasan, kecerdasan: { analisa: @kecerdasan.analisa, aritmatika: @kecerdasan.aritmatika, estimasi: @kecerdasan.estimasi, konstruktif: @kecerdasan.konstruktif, mengingat: @kecerdasan.mengingat, mengolah_angka: @kecerdasan.mengolah_angka, orientasi_ruang: @kecerdasan.orientasi_ruang, pemahaman: @kecerdasan.pemahaman, tehnik_elektronik: @kecerdasan.tehnik_elektronik, tehnik_mekanik: @kecerdasan.tehnik_mekanik, verbal: @kecerdasan.verbal, wawasan: @kecerdasan.wawasan }
    assert_redirected_to kecerdasan_path(assigns(:kecerdasan))
  end

  test "should destroy kecerdasan" do
    assert_difference('Kecerdasan.count', -1) do
      delete :destroy, id: @kecerdasan
    end

    assert_redirected_to kecerdasans_path
  end
end
