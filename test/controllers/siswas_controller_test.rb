require 'test_helper'

class SiswasControllerTest < ActionController::TestCase
  setup do
    @siswa = siswas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:siswas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create siswa" do
    assert_difference('Siswa.count') do
      post :create, siswa: { agama: @siswa.agama, berat_badan: @siswa.berat_badan, minat_kecabangan: @siswa.minat_kecabangan, nama_lengkap: @siswa.nama_lengkap, riwayat_pendidikan: @siswa.riwayat_pendidikan, tempat_lahir: @siswa.tempat_lahir, tinggi_badan: @siswa.tinggi_badan, usia: @siswa.usia }
    end

    assert_redirected_to siswa_path(assigns(:siswa))
  end

  test "should show siswa" do
    get :show, id: @siswa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @siswa
    assert_response :success
  end

  test "should update siswa" do
    patch :update, id: @siswa, siswa: { agama: @siswa.agama, berat_badan: @siswa.berat_badan, minat_kecabangan: @siswa.minat_kecabangan, nama_lengkap: @siswa.nama_lengkap, riwayat_pendidikan: @siswa.riwayat_pendidikan, tempat_lahir: @siswa.tempat_lahir, tinggi_badan: @siswa.tinggi_badan, usia: @siswa.usia }
    assert_redirected_to siswa_path(assigns(:siswa))
  end

  test "should destroy siswa" do
    assert_difference('Siswa.count', -1) do
      delete :destroy, id: @siswa
    end

    assert_redirected_to siswas_path
  end
end
