require 'test_helper'

class PembobotansControllerTest < ActionController::TestCase
  setup do
    @pembobotan = pembobotans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pembobotans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pembobotan" do
    assert_difference('Pembobotan.count') do
      post :create, pembobotan: { aspek: @pembobotan.aspek, bobot: @pembobotan.bobot, pencabangan: @pembobotan.pencabangan }
    end

    assert_redirected_to pembobotan_path(assigns(:pembobotan))
  end

  test "should show pembobotan" do
    get :show, id: @pembobotan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pembobotan
    assert_response :success
  end

  test "should update pembobotan" do
    patch :update, id: @pembobotan, pembobotan: { aspek: @pembobotan.aspek, bobot: @pembobotan.bobot, pencabangan: @pembobotan.pencabangan }
    assert_redirected_to pembobotan_path(assigns(:pembobotan))
  end

  test "should destroy pembobotan" do
    assert_difference('Pembobotan.count', -1) do
      delete :destroy, id: @pembobotan
    end

    assert_redirected_to pembobotans_path
  end
end
