require 'test_helper'

class SikapKerjasControllerTest < ActionController::TestCase
  setup do
    @sikap_kerja = sikap_kerjas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sikap_kerjas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sikap_kerja" do
    assert_difference('SikapKerja.count') do
      post :create, sikap_kerja: { daya_tahan: @sikap_kerja.daya_tahan, energi: @sikap_kerja.energi, inisiatif: @sikap_kerja.inisiatif, kecepatan: @sikap_kerja.kecepatan, kepemimpinan: @sikap_kerja.kepemimpinan, kerjasama: @sikap_kerja.kerjasama, ketekunan: @sikap_kerja.ketekunan, ketelitian: @sikap_kerja.ketelitian, keuletan: @sikap_kerja.keuletan, konsentrasi: @sikap_kerja.konsentrasi, kreatif: @sikap_kerja.kreatif, perencanaan: @sikap_kerja.perencanaan, toleransi_terhadap_stress: @sikap_kerja.toleransi_terhadap_stress }
    end

    assert_redirected_to sikap_kerja_path(assigns(:sikap_kerja))
  end

  test "should show sikap_kerja" do
    get :show, id: @sikap_kerja
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sikap_kerja
    assert_response :success
  end

  test "should update sikap_kerja" do
    patch :update, id: @sikap_kerja, sikap_kerja: { daya_tahan: @sikap_kerja.daya_tahan, energi: @sikap_kerja.energi, inisiatif: @sikap_kerja.inisiatif, kecepatan: @sikap_kerja.kecepatan, kepemimpinan: @sikap_kerja.kepemimpinan, kerjasama: @sikap_kerja.kerjasama, ketekunan: @sikap_kerja.ketekunan, ketelitian: @sikap_kerja.ketelitian, keuletan: @sikap_kerja.keuletan, konsentrasi: @sikap_kerja.konsentrasi, kreatif: @sikap_kerja.kreatif, perencanaan: @sikap_kerja.perencanaan, toleransi_terhadap_stress: @sikap_kerja.toleransi_terhadap_stress }
    assert_redirected_to sikap_kerja_path(assigns(:sikap_kerja))
  end

  test "should destroy sikap_kerja" do
    assert_difference('SikapKerja.count', -1) do
      delete :destroy, id: @sikap_kerja
    end

    assert_redirected_to sikap_kerjas_path
  end
end
