# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
User.create(id_user: 1, email: "lasiappsi@gmail.com", password: "password", nama_user: "lasiappsi", level: 1)
User.create(id_user: 2, email: "kepala@gmail.com", password: "password", nama_user: "kepala", level: 2)
User.create(id_user: 3, email: "spers@gmail.com", password: "password", nama_user: "spers", level: 3)

Pembobotan.destroy_all
Siswa.destroy_all

xlsx = Roo::Spreadsheet.open('doc/PSIKOLOGI KECABANGAN.xls.xlsx')

xlsx.sheets.each do |sheet_name|
	unless sheet_name.eql?("KRITERIA KESELURUHAN")
		xlsx.default_sheet = sheet_name
		for i in 2..xlsx.last_row
				ActiveRecord::Base.class_eval(xlsx.cell(i,1)) if xlsx.cell(i,1)
		end

	end
end