class CreateSiswas < ActiveRecord::Migration
  def change
    create_table :siswa, id: false do |t|
      t.primary_key :id_csiswa
      t.string :nama_lengkap, limit: 150
      t.string :tempat_lahir, limit: 50
      t.date :tanggal_lahir
      t.integer :tg_badan, limit: 3
      t.integer :b_badan, limit: 3
      t.integer :usia, limit: 2
      t.string :agama, limit: 10
      t.string :pendidikan, limit: 3
      t.string :minat_kec, limit: 20
      t.string :rekomendasi_kecabangan, limit: 20
      t.date :tgl_pendaftaran
      
    end
  end
end
