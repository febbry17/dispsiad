class CreateKecerdasans < ActiveRecord::Migration
  def change
    create_table :kecerdasan do |t|
      t.integer :pemahaman, limit: 3
      t.integer :mengingat, limit: 3
      t.integer :analisa, limit: 3
      t.integer :konstruktif, limit: 3
      t.integer :mengolah_angka, limit: 3
      t.integer :aritmatika, limit: 3
      t.integer :orientasi_ruang, limit: 3
      t.integer :verbal, limit: 3
      t.integer :estimasi, limit: 3
      t.integer :tehnik_mekanik, limit: 3
      t.integer :tehnik_elektronik, limit: 3
      t.integer :wawasan, limit: 3
      t.integer :id_csiswa, limit: 3

    end
  end
end
