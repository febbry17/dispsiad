class DeviseCreateUsers < ActiveRecord::Migration
  def change
    create_table :users, id: false do |t|
      ## Database authenticatable
      t.primary_key :id_user
      t.string :email, limit: 150
      t.string :encrypted_password, limit: 150
      t.string :nama_user, limit: 150
      
      t.integer :level, limit: 1
    end

    add_index :users, :email,                unique: true
    # add_index :users, :confirmation_token,   unique: true
    # add_index :users, :unlock_token,         unique: true
  end
end
