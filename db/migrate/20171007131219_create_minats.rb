class CreateMinats < ActiveRecord::Migration
  def change
    create_table :minat do |t|
      t.integer :alam_terbuka, limit: 3
      t.integer :seni, limit: 3
      t.integer :pelayanan_sosial, limit: 3
      t.integer :klerical, limit: 3
      t.integer :teknik_mekanik, limit: 3
      t.integer :hitungan, limit: 3
      t.integer :kepuasan, limit: 3
      t.integer :kepustakaan, limit: 3
      t.integer :ilmiah_sain, limit: 3
      t.integer :id_csiswa, limit: 3

    end
  end
end
