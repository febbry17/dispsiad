class CreateSikapKerjas < ActiveRecord::Migration
  def change
    create_table :sikap_kerja do |t|
      t.integer :inisiatif, limit: 3
      t.integer :kreatif, limit: 3
      t.integer :perencanaan, limit: 3
      t.integer :energi, limit: 3
      t.integer :konsentrasi, limit: 3
      t.integer :kecepatan, limit: 3
      t.integer :ketelitian, limit: 3
      t.integer :ketekunan, limit: 3
      t.integer :keuletan, limit: 3
      t.integer :daya_tahan, limit: 3
      t.integer :kepemimpinan, limit: 3
      t.integer :kerjasama, limit: 3
      t.integer :toleransi_terhadap_stress, limit: 3
      t.integer :id_csiswa, limit: 3

    end
  end
end
