class CreateKehidupanPerasaans < ActiveRecord::Migration
  def change
    create_table :kehidupan_perasaan do |t|
      t.integer :kepekaan, limit: 3
      t.integer :penempatan_diri, limit: 3
      t.integer :kepercayaan_diri, limit: 3
      t.integer :kemandirian, limit: 3
      t.integer :persuasif, limit: 3
      t.integer :agresivitas, limit: 3
      t.integer :pengelolaan_dorongan, limit: 3
      t.integer :pengelolaan_emosi, limit: 3
      t.integer :id_csiswa, limit: 3

    end
  end
end
