class CreatePembobotans < ActiveRecord::Migration
  def change
    create_table :pembobotan do |t|
      t.string :aspek, limit: 50
      t.string :pencabangan, limit: 10
      t.integer :bobot, limit: 3
      t.string :tipe, limit: 50      
    end
  end
end
