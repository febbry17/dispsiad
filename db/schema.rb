# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171007131219) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "kecerdasan", force: true do |t|
    t.integer  "pemahaman"
    t.integer  "mengingat"
    t.integer  "analisa"
    t.integer  "konstruktif"
    t.integer  "mengolah_angka"
    t.integer  "aritmatika"
    t.integer  "orientasi_ruang"
    t.integer  "verbal"
    t.integer  "estimasi"
    t.integer  "tehnik_mekanik"
    t.integer  "tehnik_elektronik"
    t.integer  "wawasan"
    t.integer  "siswa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kehidupan_perasaan", force: true do |t|
    t.integer  "kepekaan"
    t.integer  "penempatan_diri"
    t.integer  "kepercayaan_diri"
    t.integer  "kemandirian"
    t.integer  "persuasif"
    t.integer  "agresivitas"
    t.integer  "pengelolaan_dorongan"
    t.integer  "pengelolaan_emosi"
    t.integer  "siswa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "minat", force: true do |t|
    t.integer  "alam_terbuka"
    t.integer  "seni"
    t.integer  "pelayanan_sosial"
    t.integer  "klerical"
    t.integer  "teknik_mekanik"
    t.integer  "hitungan"
    t.integer  "kepuasan"
    t.integer  "kepustakaan"
    t.integer  "ilmiah_sain"
    t.integer  "siswa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sikap_kerja", force: true do |t|
    t.integer  "inisiatif"
    t.integer  "kreatif"
    t.integer  "perencanaan"
    t.integer  "energi"
    t.integer  "konsentrasi"
    t.integer  "kecepatan"
    t.integer  "ketelitian"
    t.integer  "ketekunan"
    t.integer  "keuletan"
    t.integer  "daya_tahan"
    t.integer  "kepemimpinan"
    t.integer  "kerjasama"
    t.integer  "toleransi_terhadap_stress"
    t.integer  "siswa_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "siswa", force: true do |t|
    t.string   "nama_lengkap"
    t.string   "tempat_lahir"
    t.string   "tanggal_lahir"
    t.integer  "tinggi_badan"
    t.integer  "berat_badan"
    t.integer  "usia"
    t.string   "agama"
    t.string   "riwayat_pendidikan"
    t.string   "minat_kecabangan"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "role"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
